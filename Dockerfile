FROM gcc

RUN mkdir /solver
COPY . /solver
WORKDIR /solver
RUN make
CMD ./exe 1000 10