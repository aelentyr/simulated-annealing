#ifndef SA_HPP
#define SA_HPP

#include <cstdlib>
#include <numeric>
#include <cmath>
#include <time.h>
#include "graph.hpp"

using namespace std;

typedef vector<unsigned> Solution;

class SA
{
public:
    static Solution Resolve(Graph &graph, unsigned iterMax)
    {
        srand(time(NULL));
        Solution sol = Solution(graph.nbVertices);

        //Initial set
        for_each(sol.begin(), sol.end(),
                 [](unsigned &it) { it = rand() % 2; });
        //Cooling schedule
        auto cooling = [](unsigned iter) -> double {
            static double gamma = 100;
            static double c = 2;
            return gamma / log(c + iter);
        };
        //Obj function delta
        auto dF = [&sol, &graph](unsigned node) -> int {
            static double lambda = 1.2;
            unsigned sum = 0;
            for (unsigned i = 0; i < graph.nbVertices; i++)
                sum += graph.adjacencyMatrix[node][i] && !sol[i];
            return (-1 + 2 * sol[node]) * (lambda * sum - 1);
        };

        //annealing
        unsigned iter = 0;
        double t = 0;
        unsigned nodeSelected = 0;
        double delta = 0;
        while (iter < iterMax)
        {
            iter++;
            t = cooling(iter);

            nodeSelected = rand() % graph.nbVertices;
            sol[nodeSelected] = !sol[nodeSelected]; // 0 <-> 1

            delta = dF(nodeSelected);
            if (!(delta <= 0 || rand() / RAND_MAX <= exp(-delta / t))) //If not accepted
                sol[nodeSelected] = !sol[nodeSelected];
        }

        return sol;
    }
};

#endif