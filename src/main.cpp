#include "SA.hpp"
#include <iostream>
#include <omp.h>

int main(int argc, char **argv)
{
    bool verbose = false;
    if (argc <= 2)
        return 1;

    unsigned testId = 0;
    unsigned validSolutions = 0;
    unsigned nbTests = atoi(argv[2]);
#pragma omp parallel for
    for (unsigned testId = 0; testId < nbTests; testId++)
    {
        //Creation of an instance
        unsigned nbVertices = 1 + rand() % 10;
        if (verbose)
            cout << "nbVertices = " << nbVertices << endl;
        vector<vector<unsigned>> adjacencyMatrix = vector<vector<unsigned>>(nbVertices, vector<unsigned>(nbVertices));
        for (unsigned i = 1; i < nbVertices; i++)
        {
            for (unsigned j = 0; j < i; j++)
            {
                adjacencyMatrix[i][j] = rand() % 2;
                adjacencyMatrix[j][i] = adjacencyMatrix[i][j];
                if (verbose)
                    cout << adjacencyMatrix[i][j] << " ";
            }
            if (verbose)
                cout << endl;
        }

        //SA
        Graph graph = Graph(adjacencyMatrix);
        Solution sol = SA::Resolve(graph, atoi(argv[1]));
        if (verbose)
        {
            for_each(sol.begin(), sol.end(), [](unsigned &it) { cout << it << " "; });
            cout << endl;
        }

        //Verify
        bool valid = true;
        for (unsigned i = 0; i < nbVertices; i++)
        {
            if (!sol[i] && valid)
            {
                unsigned j = -1;
                while (++j < nbVertices && (graph.adjacencyMatrix[i][j] ? sol[j] : 1)) //If stopped before nbVertice : edge i-j exist & i = j = not colored
                    ;
                if (j != nbVertices)
                    valid = false;
            }
        }
        validSolutions += valid;
    }

    cout << validSolutions * 100.0 / nbTests << "% corrects" << endl;
    return 0;
}